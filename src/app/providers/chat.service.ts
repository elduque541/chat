import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Mensaje } from '../interface/mensaje.interface';
import { map, refCount } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { reference } from '@angular/core/src/render3';
import { FirebaseDatabase } from '@angular/fire';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private itemsCollection: AngularFirestoreCollection<Mensaje>;
  private itemsCollectionA: AngularFirestoreCollection<Mensaje>;
  private itemsCollectionChats: AngularFirestoreCollection<Mensaje>;

  public chats: Mensaje[] = [];
  public usuario: any = {};

  constructor(private afs: AngularFirestore, public afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(user => {

      if (!user) {
        return;
      }

      this.usuario.nombre = user.displayName;
      this.usuario.uid = user.uid;
      this.cargarAdmin(this.usuario.uid).subscribe();
      //this.mostrarChats();
      this.get();

      console.log(this.usuario);
    });
  }

  login(proveedor: string) {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }
  logout() {
    this.usuario = {};
    this.afAuth.auth.signOut();
  }

  cargarMensajes() {
    this.itemsCollection = this.afs.collection<Mensaje>('chats').doc('chat').collection<Mensaje>(
      this.usuario.uid, ref => ref.orderBy('fecha', 'desc')
        .limit(5));

    return this.itemsCollection.valueChanges().pipe(
      map((mensajes: Mensaje[]) => {
        console.log(mensajes);

        this.chats = [];
        for (let mensaje of mensajes) {
          this.chats.unshift(mensaje);
        }

        return this.chats;

      })
    );
  }

  cargarMensajesAdmin() {
    this.itemsCollection = this.afs.collection<any>('admins');

    return this.itemsCollection.valueChanges().pipe(
      map((mensajes: Mensaje[]) => {
        //  console.log(mensajes, 'raiz');

        this.chats = [];
        for (let mensaje of mensajes) {
          this.chats.unshift(mensaje);
        }

        return this.chats;

      })
    );
  }

  mostrarChats() {


    const docRef2 = this.afs.collection<any>('chats').snapshotChanges()
      .pipe(
      ).subscribe((j) => {
        console.log(j, 'iddddddddddddd');
      });

    console.log(docRef2, 'docRef');
  }


  cargarAdmin(idUsuario: string) {
    this.itemsCollectionA = this.afs.collection<any>('admins');

    return this.itemsCollectionA.valueChanges().pipe(
      map((mensajes: any[]) => {

        //  console.log(mensajes, 'raizAdmins');

        for (let mensaje of mensajes) {
          if (mensaje.id === idUsuario) {
            this.usuario.admin = true;
            break;
          }
        }
      }));
  }

  agregarMensaje(texto: string) {
    // TODO UID
    let mensaje: Mensaje = {
      nombre: this.usuario.nombre,
      mensaje: texto,
      fecha: new Date().getTime(),
      uid: this.usuario.uid
    }

    


    return this.itemsCollection.add(mensaje);
  }

  get() {
    return this.afs
      .collection<any>('chats')
      .doc('chat')
      .snapshotChanges()
      .pipe(
        map(doc => {
          console.log(doc, 'doc');
          return { id: doc.payload.id, ...doc.payload.data() };
        })
      );
  }
  
}