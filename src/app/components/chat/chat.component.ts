import { Component, OnInit } from '@angular/core';

//servicio
import { ChatService } from '../../providers/chat.service';
import { element } from 'protractor';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styles: []
})
export class ChatComponent implements OnInit {

  mensaje = '';
  elemento: any;

  ngOnInit(): void {
    this.elemento = document.getElementById('app-mensajes');
  }

  constructor(public _cs: ChatService) {
    this._cs.cargarMensajes().subscribe(
      () => {
        setTimeout(() => {
          this.elemento.scrollTop = this.elemento.scrollHeight;
        }, 20);
      }
    );

  }

  enviar_mensaje() {
    if (this.mensaje.length === 0) {
      return ;
    }
    this._cs.agregarMensaje(this.mensaje)
      .then(() => this.mensaje = '')
      .catch((err) => console.error('error', err));
    console.log(this.mensaje);
  }
}
